var fs = require("fs");
const superagent = require("superagent");

// Callback

/*fs.readFile("./robohash.txt", (err, data) => {
  console.log(`The robo name is ${data}`);
  superagent.get(`https://robohash.org/${data}`).end((err, res) => {
    console.log(res.request.url);
    fs.writeFile("./robotimage.txt", res.request.url, (err) => {
      console.log("Robot Succesfully written in file");
    });
  });
});*/

// Promises function

// function readFilePromise(fileLocation) {
//   return new Promise((resolve, reject) => {
//     fs.readFile(fileLocation, (err, randomText) => {
//       if (err) {
//         reject("not able to read the file");
//       }
//       resolve(randomText);
//     });
//   });
// }
// function writeFilePromise(fileLocation, result) {
//   return new Promise((resolve, reject) => {
//     fs.writeFile(fileLocation, result, (err) => {
//       if (err) {
//         reject("not able to write to the file");
//       }
//       resolve();
//     });
//   });
// }

// readFilePromise("./robohash.txt")
//   .then((randomText) => {
//     console.log(`my string is ${randomText}`);
//     return superagent.get(`https://robohash.org/${randomText}/images/random`);
//   })
//   .then((res) => {
//     console.log("String  is ", res.request.url);
//     return writeFilePromise("./robotimage.txt", res.request.url);
//   })
//   .then(() => {
//     console.log("sucessfully written the file");
//   })
//   .catch((err) => {
//     console.log(err);
//   });

// Async await
const data = Math.random().toString(36).substring(2, 7);

function RandomString(data) {
  console.log(`Random string is :${data}`);
}

function writeFilePromise(filelocation, data) {
  return new Promise((resolve, reject) => {
    fs.writeFile(filelocation, data, (err) => {
      if (err) {
        reject("not able to write content in file");
      }
      resolve(data);
    });
  });
}

async function robohash() {
  try {
    RandomString(data);
    const res = await superagent.get(`https://robohash.org/${data}`);
    console.log(res.request.url);
    writeFilePromise("./robotimage.txt", res.request.url);
    console.log(typeof data);
    console.log("image saved successfully!");
  } catch (err) {
    console.log(err);
  }
}

robohash();
